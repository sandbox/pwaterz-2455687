This module allows developers to hard code cache configurations into ctools content types.
The settings are dependent on the panels caching method that is desired.

Example:

$plugin = array(
  'single' => TRUE,
  'icon' => 'icon_node_form.png',
  'title' => t('Auth String'),
  'description' => t('Provides a simple auth string'),
  'category' => t('HighWire'),
  'defaults' => array(),


  'ctools_content_type_cache' => TRUE, // Allows for enabling and disabling of these settings
  'ctools_content_type_cache_method' => 'simple', // Set the panels cache method. This is determined by the panels cache plugin desired.
  'ctools_content_type_cache_settings' => array('lifetime' => 1500, 'granularity' => 'context'), // Settings required based on the panels caching plugin
);

Figuring out what settings are needed for ctools_content_type_cache_settings is a bit of a black box.
A use full way to figure out what the setting should be is to do the following
1. Go to a pane and use the UI to add desired cache settings you would like.
2. Enabled the devel module and go to devel/php.
3. Enter the following code into the textarea:

// Now check all panel pages
ctools_include('page', 'page_manager', 'plugins/tasks');
ctools_include('page_manager.admin', 'page_manager', '');
ctools_include('export');

$tasks = page_manager_get_tasks_by_type('page');
$page_types = array();

foreach ($tasks as $task) {
  // Disabled page return empty
  if ($pages = page_manager_load_task_handlers($task)) {
    $page_types[] = $pages;
  }
}

// Not all display objects are loaded, make sure to load them
foreach ($page_types as &$pages) {
  foreach ($pages as &$page) {
    if (empty($page->conf['display']) && !empty($page->conf['did'])) {
      $page->conf['display'] = panels_load_display($page->conf['did']);
    }
  }
}

dsm($page_types);

4. This will load all panel page in the site and dsm the their configurations.
   You'll want to look under [page name] -> ['conf'] -> ['display'] -> ['content'] -> [pane] -> ['cache'].
   This will show you the settings you need to use for ctools_content_type_cache_settings and ctools_content_type_cache_method
